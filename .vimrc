filetype off

call plug#begin()
Plug 'dmerejkowsky/vim-ale' "syntax highlighting
Plug 'tpope/vim-surround' "matching brackets in codec
Plug 'python-rope/ropevim' "python refactoring
Plug 'python-mode/python-mode' "vim > python IDE
call plug#end()

syntax on "show syntax in files if possible

"enable file type detection
filetype plugin on
filetype indent on

set number "show numbers on lines
set cursorline "line showing where the cursor is
set showmode "show the mode vim is currently in

set tabstop=4 "pressing tab creates 4 spaces
set softtabstop=4 "the number of spaces tab uses for operations
set shiftwidth=4 "pressing >> or << indents by 4 spaces
set expandtab "convert tabs to spaces

set relativenumber "show relative number in sidebar (left)
set history=1000 "number of commands to save
set noswapfile "disables swap files, if it crashes, it crashes
set spell "enable spellchecking

set foldmethod=indent "fold on indent, not brackets
set foldnestmax=3
set foldlevel=99 "don't fold files by default
nnoremap <space> za
vnoremap <space> zf

set textwidth=80

set showmatch "show matching brackets

let g:netrw_banner=0 "don't show netrw banner
let g:netrw_list_hide='\./' "hide ./ and ../
let g:netrw_hide=1

colorscheme desert
highlight LineNr ctermfg=lightgrey
set colorcolumn=80
highlight ColorColumn ctermbg=lightblue

"USEFUL COMMANDS
":%!black - -q     <- runs black on the currently open file
"<c-w> o           <- makes the current panel fullscreen
"<c-w> w           <- change to other panel
