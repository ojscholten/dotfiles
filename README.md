# dotfiles

This repository contains useful dotfiles (configuration files) for bash (.zshrc)
and vim (.vimrc).

See the [vim documentation](https://vimdoc.sourceforge.net/htmldoc/usr_toc.html) for a comprehensive read through all of vim's features and functionality.

The [vimcolors.org](https://vimcolors.org/) site may be useful for creating your
own color scheme. The best examples find a picture which contains the colors
they like, and samples it for colors.

This [guide to using netrw](https://vonheikemen.github.io/devlog/tools/using-netrw-vim-builtin-file-explorer/) is a useful reference document.

The [GNU Bash Manual](https://www.gnu.org/software/bash/manual/bash.html) is a comprehensive guide to bash. The rest of the [GNU manuals](https://www.gnu.org/manual/manual.html) contain a wealth of information across a range of tools.



